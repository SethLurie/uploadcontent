import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-university-content-form',
  templateUrl: './university-content-form.component.html',
  styleUrls: ['./university-content-form.component.css']
})
export class UniversityContentFormComponent implements OnInit {
  location : String;
  date : String;
  link : String;
  nbt : String;
  qualifications : String[]
  quals : any[];
  numQuals : number;

  constructor() { }

  ngOnInit() {
    this.nbt = "Yes";
    this.numQuals = 0;
    this.qualifications = [];
    this.quals = [];
    this.location = "";
    this.date = "";
    this.link = "";
  }

  getData(){
    this.numQuals = 0;
    return {location: this.location,
            applicationCLosingDate: this.date,
            link: this.link,
            NBTRequirement : this.nbt,
            qualifications : this.qualifications
            };
  }

  addQual(){
    this.numQuals ++;
    this.qualifications.push("");
  }

  removeQual(){
    if (this.numQuals > 0 ){
      this.qualifications.splice(this.numQuals-1,1);
      this.numQuals--;
    }
  }

  trackByFn(index, item) {
    return item.id;
  }

}
