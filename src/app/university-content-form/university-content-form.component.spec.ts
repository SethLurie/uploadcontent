import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversityContentFormComponent } from './university-content-form.component';

describe('UniversityContentFormComponent', () => {
  let component: UniversityContentFormComponent;
  let fixture: ComponentFixture<UniversityContentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityContentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversityContentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
