import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService}from './auth.service';
import { AngularFireStorage } from 'angularfire2/storage';


@Injectable()
export class FirebaseService {
  universities;
  resources;
  bursaries;
  db;

  constructor(db : AngularFireDatabase, private auth: AuthService, private afStorage: AngularFireStorage) {
    auth.loginWithEmail("setolurie@gmail.com","Seth123");
    this.db = db;
    this.universities = db.list("/Universities");
    this.bursaries = db.list("/Applications");
    this.resources = db.list("/Resources");
  }

  getUniversities() {
    return this.universities;
  }

  getBursaries(){
    return this.bursaries;
  }

  getResources(){
    return this.resources;
  }

  saveUniversity(university:any){
    var idn = this.universities.push(university).key;
    console.log(idn);
    this.db.object("/Universities/" + idn).update ({id :idn});
    return "/Universities/" + idn;
  }

  saveBursary(bursary:any){
    var idn = this.bursaries.push(bursary).key;
    console.log(idn)
    this.db.object("/Applications/"+idn).update({id:idn});
    return "/Applications/"+ idn;
  }

  saveResource(resource:any){
    var idn = this.resources.push(resource).key;
    console.log(idn)
    this.db.object("/Resources/"+idn).update({id:idn});
    return "/Resources/"+idn;
  }

  savePic(pic : any, id){
    this.afStorage.upload(id, pic);
  }

}
