import { Component, OnInit, ViewChild } from '@angular/core';
import { UniversityContentFormComponent } from '../university-content-form/university-content-form.component';
import { FirebaseService } from '../firebase.service';
import { BursaryContentFormComponent} from '../bursary-content-form/bursary-content-form.component';
import { ResourceComponent} from '../resource/resource.component';

@Component({
  selector: 'app-content-form',
  templateUrl: './content-form.component.html',
  styleUrls: ['./content-form.component.css'],
})
export class ContentFormComponent implements OnInit {
  @ViewChild(UniversityContentFormComponent) universityForm :UniversityContentFormComponent;
  @ViewChild(BursaryContentFormComponent) bursaryForm : BursaryContentFormComponent;
  @ViewChild(ResourceComponent) resourceForm: ResourceComponent;

  type :String;
  title : String;
  subtitle : String;
  otherFieldData : any;
  submit = false;
  obj : any;
  pic : any;
  path: String;

  constructor(private fs : FirebaseService) {
   }

  ngOnInit() {
    this.type = "Bursary";
    this.otherFieldData = {};
    this.obj = {};
    this.pic = null;
    this.path ="";
  }

  onSubmit (){
    switch(this.type){
      case "University" :{
        this.otherFieldData = this.universityForm.getData();
        break;
      }

      case "Bursary" :{
        this.otherFieldData = this.bursaryForm.getData();
        break;
      }

      default:{
        this.otherFieldData = this.resourceForm.getData();
        break;
      }
    };

    this.obj = {title: this.title,
          subtitle: this.subtitle};

    switch(this.type){
        case "University" :{
          this.path = this.fs.saveUniversity({...this.obj, ...this.otherFieldData});
          break;
        }

        case "Bursary":{
          this.path = this.fs.saveBursary({...this.obj, ...this.otherFieldData});
          break;
        }
        default:{
          this.path = this.fs.saveResource({...this.obj, ...this.otherFieldData}) ;
        }
    };


    this.submit = true;
    this.fs.savePic(this.pic,this.path + ".jpg");
  }

  up(event){
    this.pic = event.target.files[0]
  }
}
