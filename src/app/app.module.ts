import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireAuth} from 'angularfire2/auth';
import { AngularFireStorage } from 'angularfire2/storage';

import { AppComponent } from './app.component';
import { ContentFormComponent } from './content-form/content-form.component';
import { UniversityContentFormComponent } from './university-content-form/university-content-form.component';
import { FirebaseService } from './firebase.service';
import { AuthService} from './auth.service';
import { BursaryContentFormComponent } from './bursary-content-form/bursary-content-form.component';
import { ResourceComponent } from './resource/resource.component';




@NgModule({
  declarations: [
    AppComponent,
    ContentFormComponent,
    UniversityContentFormComponent,
    BursaryContentFormComponent,
    ResourceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'test'),
    AngularFireDatabaseModule
  ],
  providers: [FirebaseService, AuthService, AngularFireAuth, AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
