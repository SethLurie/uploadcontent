import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.css']
})
export class ResourceComponent implements OnInit {

  text : String;
  field : String;

  constructor() { }

  ngOnInit() {
    this.text = "";
    this.field = "";
  }

  getData(){
    return {text : this.text, field: this.field};
  }

}
