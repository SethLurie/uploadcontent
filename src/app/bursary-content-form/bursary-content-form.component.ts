import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bursary-content-form',
  templateUrl: './bursary-content-form.component.html',
  styleUrls: ['./bursary-content-form.component.css']
})
export class BursaryContentFormComponent implements OnInit {
  applicableFields : String[];
  numFields : number;
  requirements : String[];
  numReqs : number;
  support : String;
  numSup : number
  instructions : String;
  link : String;
  closingDate : String;
  compId : String;
  compName : String;
  field : String ;

  constructor() { }

  ngOnInit() {
    this.requirements =[];
    this.numReqs = 0;
    this.support = "";
    this.numSup = 0;
    this.applicableFields = [];
    this.numFields = 0;
    this.instructions = "";
    this.link = "";
    this.closingDate = "";
    this.compId ="";
    this.compName = "";
    this.field = "";
  }

  addField(){
    this.numFields++;
    this.applicableFields.push("");
  }

  removeField(){
    if(this.numFields >0){
        this.applicableFields.splice(this.numFields-1, 1);
        this.numFields--;
    }
  }

  addReq(){
    this.numReqs++;
    this.requirements.push("");
  }

  removeReq(){
    if(this.numReqs >0){
        this.requirements.splice(this.numFields-1, 1);
        this.numReqs--;
    }
  }


  trackByFn(index, item) {
    return item.id;
  }

  getData(){
    return {applicableFields : this.applicableFields,
            applicaitonProcess : this.instructions,
            bursaryUrl : this.link,
            closingDate : this.closingDate,
            field: this.field,
            requirements : this.requirements,
            supportProvided : this.support
            }
  }

}
