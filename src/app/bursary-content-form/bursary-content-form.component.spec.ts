import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BursaryContentFormComponent } from './bursary-content-form.component';

describe('BursaryContentFormComponent', () => {
  let component: BursaryContentFormComponent;
  let fixture: ComponentFixture<BursaryContentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BursaryContentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BursaryContentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
